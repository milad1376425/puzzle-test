1. Drag VisualSetting prefab to a scene
2. Link skymat to lighting > Environment > SkyMaterial
3. Link Main Light to "Visual Settings" prefab

4. Usage in your main Game Manager:

using PurpleMonster.Visual;
 private void Awake()
    {
        gameVisualSettings.SetupVisual();
    }