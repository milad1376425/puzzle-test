using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PurpleMonster.Visual
{
    public class VisualSettings : MonoBehaviour
    {
        [SerializeField] VisualData[] visualDatas;
        private int currentVisualData;

        public int CurrentVisualData => currentVisualData;
        public int VisualDataLenght => visualDatas.Length - 1;


        [Space(20)]
        [Header("Reference")]
        [SerializeField] Material skyMat;
        [SerializeField] Light dlight;


        public VisualData VisualData => visualDatas[currentVisualData];
        public VisualData RandomVisualData => visualDatas[RandomData];
        int RandomData => Random.Range(0, visualDatas.Length);

        public Light Scenelight { get => dlight; set => dlight = value; }

        public void SetSkyMat(Color colorA, Color colorB)
        {
            visualDatas[currentVisualData].SetSkyMat(colorA, colorB, skyMat);
        }

        public void SetupVisual()
        {
            VisualData myVisualData = RandomVisualData;
            myVisualData.SetSkyMat(skyMat);
            RenderSettings.fogColor = myVisualData.FogColor;
            RenderSettings.ambientLight = myVisualData.EnvColor;
            if (Scenelight) Scenelight.color = myVisualData.LightColor;
            if (Scenelight) Scenelight.transform.eulerAngles = myVisualData.LightAngle;
        }

        public void SetupVisualEditor()
        {
            VisualData myVisualData = VisualData;
            myVisualData.SetSkyMat(skyMat);
            RenderSettings.fogColor = myVisualData.FogColor;
            RenderSettings.ambientLight = myVisualData.EnvColor;
            if (Scenelight) Scenelight.color = myVisualData.LightColor;
            if (Scenelight) Scenelight.transform.eulerAngles = myVisualData.LightAngle;
        }
        public void SetLightAngle(Vector3 angle) { Scenelight.transform.eulerAngles = angle; }

        public void SetCurrentVisualData(int currentVisualData)
        {
            this.currentVisualData = currentVisualData;
        }
    }
}