using UnityEngine;
using PurpleMonster.Visual;

public class SampleOfUsage : MonoBehaviour
{
    [SerializeField] VisualSettings gameVisualSettings;

    private void Awake()
    {
        // setup random scene visual
        gameVisualSettings.SetupVisual();
    }
}
