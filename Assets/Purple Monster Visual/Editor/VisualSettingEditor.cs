using UnityEngine;
using UnityEditor;

namespace PurpleMonster.Visual
{
    [CustomEditor(typeof(VisualSettings))]
    public class VisualSettingEditor : Editor
    {
        VisualSettings myScript;
        private Color fogColor, skyColor1, skyColor2, lightColor;
        [ColorUsage(true, true)] private Color envColor;
        int currentVisualData;
        int visualDataLenght;
        float lightY, lightX;
        Vector3 lightAngle;
        private void OnEnable()
        {
            myScript = (VisualSettings)target;
            if (myScript.VisualDataLenght > -1)
            {
                currentVisualData = myScript.CurrentVisualData;
                visualDataLenght = myScript.VisualDataLenght;
                Load();
            }
        }

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("New Game"))
            {
                PlayerPrefs.DeleteAll();
            }


            Color oldEnv = envColor;
            GUIContent content = new GUIContent("Environment Color");
            envColor = EditorGUILayout.ColorField(content, envColor, true, false, true);
            if (envColor != oldEnv)
            {
                myScript.VisualData.SetEnvColor(envColor);
                RenderSettings.ambientLight = envColor;
                SceneView.RepaintAll();
                EditorUtility.SetDirty(myScript.VisualData);
            }




            Color oldskycolor2 = skyColor2;
            skyColor2 = EditorGUILayout.ColorField("Sky Color Up", skyColor2);
            if (oldskycolor2 != skyColor2)
            {
                myScript.SetSkyMat(skyColor1, skyColor2);
                SceneView.RepaintAll();
                EditorUtility.SetDirty(myScript.VisualData);
            }


            Color oldskycolor1 = skyColor1;
            skyColor1 = EditorGUILayout.ColorField("Sky Color Down", skyColor1);
            if (oldskycolor1 != skyColor1)
            {
                myScript.SetSkyMat(skyColor1, skyColor2);
                SceneView.RepaintAll();
                EditorUtility.SetDirty(myScript.VisualData);
            }

            Color oldColor = fogColor;
            fogColor = EditorGUILayout.ColorField("Fog Color", fogColor);
            if (oldColor != fogColor)
            {
                myScript.VisualData.SetFogColor(fogColor);
                RenderSettings.fogColor = fogColor;
                SceneView.RepaintAll();
                EditorUtility.SetDirty(myScript.VisualData);
            }

            Color oldLightColor = lightColor;
            lightColor = EditorGUILayout.ColorField("Light Color", lightColor);

            if (oldLightColor != lightColor)
            {
                myScript.VisualData.LightColor = lightColor;
                myScript.Scenelight.color = lightColor;
                SceneView.RepaintAll();
                EditorUtility.SetDirty(myScript.VisualData);
            }


            float oldLightAngleY = lightY;
            lightY = EditorGUILayout.Slider("Light Rotation", lightY, 0, 360);
            if (oldLightAngleY != lightY)
            {
                lightAngle = new Vector3(lightX, lightY, 0);
                myScript.VisualData.LightAngle = lightAngle;
                myScript.SetLightAngle(lightAngle);
                EditorUtility.SetDirty(myScript.VisualData);
            }

            float oldLightAngleX = lightX;
            lightX = EditorGUILayout.Slider("Light Tilt", lightX, 0, 90);
            if (oldLightAngleX != lightX)
            {
                lightAngle = new Vector3(lightX, lightY, 0);
                myScript.VisualData.LightAngle = lightAngle;
                myScript.SetLightAngle(lightAngle);
                EditorUtility.SetDirty(myScript.VisualData);
            }


            int oldCurrentVisualData = currentVisualData;
            currentVisualData = Mathf.FloorToInt(
                EditorGUILayout.Slider("Change Data", currentVisualData, 0, visualDataLenght));
            if (oldCurrentVisualData != currentVisualData)
                Refresh();

            base.DrawDefaultInspector();
        }

        void Refresh()
        {
            myScript.SetCurrentVisualData(currentVisualData);
            visualDataLenght = myScript.VisualDataLenght;
            Load();
            myScript.SetupVisualEditor();
            EditorUtility.SetDirty(myScript.VisualData);
        }

        void Load()
        {

            fogColor = myScript.VisualData.FogColor;
            envColor = myScript.VisualData.EnvColor;
            skyColor1 = myScript.VisualData.SkyColor1;
            skyColor2 = myScript.VisualData.SkyColor2;
            lightColor = myScript.VisualData.LightColor;
            lightAngle = myScript.VisualData.LightAngle;

            lightY = lightAngle.y;
            lightX = lightAngle.x;
        }
    }
}