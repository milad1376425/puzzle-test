using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PurpleMonster.Visual
{
    [CreateAssetMenu(fileName = "Visual_Data_", menuName = "Visual Data", order = 1)]
    public class VisualData : ScriptableObject
    {
        [SerializeField] Color fogColor;
        [ColorUsage(true, true)] [SerializeField] Color envColor;
        [SerializeField] Color color1;
        [SerializeField] Color color2;
        [SerializeField] Color lightColor = new Color(1f, .6f, 0.3f, 1f);
        [SerializeField] Vector3 lightAngle;
        public VisualData()
        {
            fogColor = new Color(0.11f, .7f, 1f, 1f);
            envColor = new Color(.6f, .8f, .5f, 1f);
            color1 = new Color(1f, 1f, 0.6f, 1f);
            color2 = new Color(0.11f, 1f, 0.72f, 1f);
        }

        public Color FogColor => fogColor;


        public void SetFogColor(Color color)
        {
            fogColor = color;
        }


        public Color EnvColor => envColor;
        public void SetEnvColor(Color color)
        {
            envColor = color;
        }


        public Color SkyColor1 => color1;
        public Color SkyColor2 => color2;

        public Color LightColor { get => lightColor; set => lightColor = value; }
        public Vector3 LightAngle { get => lightAngle; set => lightAngle = value; }

        public void SetSkyMat(Color a, Color b, Material skyMat)
        {
            color1 = a;
            color2 = b;
            skyMat.SetColor("Color2", a);
            skyMat.SetColor("Color1", b);

        }

        public void SetSkyMat(Material skyMat)
        {
            skyMat.SetColor("Color2", color1);
            skyMat.SetColor("Color1", color2);
        }
    }


}