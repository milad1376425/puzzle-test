using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Hole : MonoBehaviour
{
    //public StatsScriptableObject stat;
    public PieceType type;
    public AttributeScriptableObject Attribute;
    private GameManager gameManager;


    public bool method2;
    public float directionSpeed;
    public float speedReduceTime;
    private GameObject piece;

    public bool reached = false;
    public Vector3 reachPosition;
    public Vector3 direction;
    public float speed;
    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }
    void Start()
    {
        reachPosition = new Vector3(transform.position.x, 0.1f, transform.position.z);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Piece"))
        {
            var rigidbody = other.transform.root.GetComponent<Rigidbody>();
            var part = other.transform.root.GetComponent<Shooting>();
            if (!reached && part.type == type && rigidbody.velocity.magnitude < 15 && rigidbody.velocity.magnitude > 1)
            {
                part.catched = true;
                other.isTrigger = true;
                other.tag = "Filled";
                GetComponent<Collider>().enabled = false;
                direction = rigidbody.velocity.normalized;
                speed = rigidbody.velocity.magnitude;
                rigidbody.velocity = Vector3.zero;
                reached = true;

                StartCoroutine(Tornado(other.transform.root.gameObject, rigidbody));
                piece = other.transform.root.gameObject;
            }
        }
    }
    IEnumerator Tornado(GameObject Piece, Rigidbody rigidbody)
    {
        if (Piece.transform.rotation.eulerAngles != Vector3.zero)
        {
            rigidbody.angularVelocity = Vector3.zero;
            Piece.transform.DORotate(Vector3.zero, 100, RotateMode.Fast).SetSpeedBased().SetEase(Ease.InOutQuad);
        }
        while (Vector3.Distance(Piece.transform.position, reachPosition) > 0.3 && speed != 0)
        {
            direction = Vector3.MoveTowards(direction, (reachPosition - Piece.transform.position).normalized, Time.deltaTime * speed / 3);
            //Piece.transform.position = Vector3.MoveTowards(Piece.transform.position, direction, Time.deltaTime * speed);
            rigidbody.velocity = direction * speed;
            //Piece.transform.position += (reachPosition - Piece.transform.position).normalized * Time.deltaTime;
            //Piece.transform.RotateAround(transform.position, Vector3.up, Time.deltaTime * -100);
            //Piece.transform.Translate(direction * Time.deltaTime * speed);
            //rigidbody.AddForce(direction*speed);
            speed *= speedReduceTime;
            if (speed <= 0.5) speed = 0.5f;
            yield return null;
        }
        rigidbody.velocity = Vector3.zero;
        Piece.transform.DOMove(reachPosition, 0.5f).SetEase(Ease.OutQuad);
        //if (Piece.transform.rotation.eulerAngles == Vector3.zero)
            Fall(Piece, rigidbody);
    }
    private void Fall(GameObject other, Rigidbody rigidbody)
    {
        if (method2)
        {
            other.GetComponent<Shooting>().anim.SetTrigger("fall");
            rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        }
        else
        {
            rigidbody.useGravity = true;
            rigidbody.constraints = ~RigidbodyConstraints.FreezePositionY;
        }
        gameManager.AddFillHoles(other.transform);
    }
    private void OnDrawGizmos()
    {
        if (piece)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(piece.transform.position, direction * 10);
        }
    }
}
