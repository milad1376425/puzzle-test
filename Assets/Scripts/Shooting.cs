using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Shooting : MonoBehaviour
{
    //public StatsScriptableObject stat;
    public PieceType type;
    public bool catched;
    public Animator anim;
    private float shootPower;


    private GameManager gameManager;
    private Rigidbody rb;
    private GameObject mousepointA;
    private GameObject mousepointB;
    private GameObject mousepointC;
    private ParticleSystem Arrow;
    private ParticleSystem Arrow2;
    private ParticleSystem Portal;

    public bool allowedToShoot;
    private Vector3 shootDirection;
    private bool reflected;
    private float reflectPower;
    private float currentDistance;
    private float allowedDistance;
    public float maxDistance = 3f;


    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        rb = GetComponent<Rigidbody>();
        mousepointA = GameObject.FindGameObjectWithTag("PointA");
        mousepointB = GameObject.FindGameObjectWithTag("PointB");
        mousepointC = GameObject.FindGameObjectWithTag("PointC");
        Arrow = GameObject.FindGameObjectWithTag("Arrow").GetComponent<ParticleSystem>();
        Arrow2 = GameObject.FindGameObjectWithTag("Arrow2").GetComponent<ParticleSystem>();
        Portal = GameObject.FindGameObjectWithTag("Portal").GetComponent<ParticleSystem>();
        StartWaitingMethod();
    }
    private void OnMouseDrag()
    {
        if (allowedToShoot)
        {
            anim.applyRootMotion = true;
            Vector3 transformWithOutY = new Vector3(transform.position.x, 1, transform.position.z);
            currentDistance = Vector3.Distance(mousepointA.transform.position, transformWithOutY);
            if (currentDistance > maxDistance)
            {
                allowedDistance = maxDistance;
            }
            else
            {
                allowedDistance = currentDistance;
            }
            shootPower = Mathf.Abs(allowedDistance) * 5;

            Vector3 distanceAndDirection = mousepointA.transform.position - transformWithOutY;
            //magnitude yani andaze va meghdare bordar (vector.distance ham hamine vali inja baraye khonsa kardane direction hardoro mikhaim)
            float magnitudeOfD = distanceAndDirection.magnitude;

            //inja direction ro taghsimbar andazash mikonim ke khonsa beshe va tasiri toye meghdari ke mikhahim nadashte bashe va baraye fasele faghat az currentDistance estefade konim.
            //va be in dalil inkaro kardim chon direction hast va jahate (y) ham baraks mishod pas yekishod bala miraf yekishon paeen, pas bayad az directionAndDistance faghat baraye jahat estefade konim na fasele.
            //nokteye mohem inke in formule ba normalized farghi nadare va mitavanim az normalaize ham estefade konim.

            RaycastHit hit;

            if (Physics.Raycast(transform.position, -distanceAndDirection.normalized, out hit, allowedDistance, 1 << 6))
            {
                // Find the line from the gun to the point that was clicked.
                Vector3 incomingVec = hit.point - transformWithOutY;

                // Use the point's normal to calculate the reflection vector.
                Vector3 reflectVec = Vector3.Reflect(incomingVec, hit.normal);

                // Draw lines to show the incoming "beam" and the reflection.
                Debug.DrawLine(transform.position, hit.point, Color.red, 0, false);
                Debug.DrawRay(hit.point, reflectVec, Color.green, 0);
                mousepointB.transform.position = hit.point;
                Reflect(reflectVec, hit.point);

            }
            else
            {
                reflected = false;
                Arrow2.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
                mousepointB.transform.position = transform.position + -distanceAndDirection / magnitudeOfD * allowedDistance;

            }

            //mousepointB.transform.position = hit.point;
            mousepointB.transform.position = new Vector3(
                Mathf.Clamp(mousepointB.transform.position.x, -5, 5),
                1,
                Mathf.Clamp(mousepointB.transform.position.z, -7, 8.5f)
            );

            shootDirection = Vector3.Normalize(distanceAndDirection);

            ArrowPoint(Arrow, transform.position, Vector3.Distance(mousepointB.transform.position, transformWithOutY), mousepointB);
        }
    }
    private void OnMouseUp()
    {
        if (allowedToShoot)
        {
            var shoot = shootDirection * shootPower * -1;
            rb.AddForce(shoot, ForceMode.Impulse);
            Arrow.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
            Arrow2.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
            allowedToShoot = false;
            gameManager.StartShooting();
            StartCoroutine(NotCatched());
        }
    }
    private void ArrowPoint(ParticleSystem Arrow, Vector3 position, float distance, GameObject point)
    {
        Arrow.gameObject.transform.position = position + Vector3.up;
        Arrow.gameObject.transform.LookAt(point.transform);
        Arrow.gameObject.transform.rotation = Quaternion.Euler(0, Arrow.gameObject.transform.rotation.eulerAngles.y, 0);
        var main = Arrow.main;
        if (main.startLifetime.constant != distance)
        {
            Arrow.Stop();
            main.startLifetime = distance;
            Arrow.Play();
        }
    }

    private void Reflect(Vector3 reflect, Vector3 reflectPos)
    {
        if (!reflected)
        {
            reflectPower = allowedDistance;
            reflected = true;
        }
        var directionPower = allowedDistance - reflectPower;
        mousepointC.transform.position = reflectPos + reflect * directionPower;
        mousepointC.transform.position = new Vector3(mousepointC.transform.position.x, 1, mousepointC.transform.position.z);
        ArrowPoint(Arrow2, reflectPos, directionPower * 2, mousepointC);
    }
    public void StartWaitingMethod()
    {
        allowedToShoot = false;
        StartCoroutine(WaitingAnimaton());
    }
    IEnumerator WaitingAnimaton()
    {
        yield return new WaitForSeconds(2);
        while (!allowedToShoot)
        {
            float randomPercent = Random.value * 100;
            if (randomPercent < 20)
            {
                var randomAnim = Random.Range(0, 7);
                switch (randomAnim)
                {
                    case 0:
                        anim.SetTrigger("Jump");
                        break;
                    case 1:
                        anim.SetTrigger("Jump2");
                        break;
                    //case 2:
                    //    anim.SetTrigger("Jump3");
                    //    break;
                    case 3:
                        anim.SetTrigger("Jump4");
                        break;
                    case 4:
                        anim.SetTrigger("Rotate");
                        break;
                    case 5:
                        anim.SetTrigger("Stumble");
                        break;
                }
            }
            yield return new WaitForSeconds(Random.Range(2, 4));
        }
    }
    IEnumerator NotCatched()
    {
        yield return new WaitForSeconds(2f);
        while(!catched)
        {
            if(rb.velocity.magnitude<=0.1f)
            {
                rb.isKinematic = true;
                Portal.transform.position = transform.position;
                Portal.transform.DOScale(1, 0.5f);
                transform.DOMoveY(1, 0.5f).SetEase(Ease.InOutQuad).OnComplete(()=> transform.DOMoveY(-2, 0.5f).SetEase(Ease.InOutQuad));
                Portal.transform.DOScale(0, 0.5f).SetDelay(1f);
                transform.DOScale(0, 0.1f).SetDelay(1);
                yield return new WaitForSeconds(1f);
                gameManager.PushtoQueue(gameObject);
                yield break;
            }
            yield return null;
        }
    }
}
