using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Piece",menuName ="Puzzle Piece")]
public class AttributeScriptableObject : ScriptableObject
{
    public int Selected;
    public Attributes[] myType;

    public Texture2D WhichSelected()
    {
        return myType[Selected].icon;
    }

    public Texture2D SelectNext()
    {
        if (Selected + 1 >= myType.Length)
            Selected = 0;
        else
            Selected++;

        return myType[Selected].icon;
    }

    public GameObject GetPrefab(int number)
    {
        return myType[number].Prefab;
    }
    public GameObject GetHolePrefab(int number)
    {
        return myType[number].HolePrefab;
    }
}
[System.Serializable]
public class Attributes
{
    public PieceType type;
    public GameObject Prefab;
    public GameObject HolePrefab;
    public Texture2D icon;
}
