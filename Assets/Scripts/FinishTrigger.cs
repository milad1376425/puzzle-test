using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Filled"))
        {
            var anim= other.transform.parent.GetComponent<Animator>();
            anim.Rebind();
            if (!anim.enabled) anim.enabled = true;
            anim.SetTrigger("GetOut");

        }
    }
}
