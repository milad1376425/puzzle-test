using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
public class GameManager : MonoBehaviour
{
    public static bool Finished;

    public List<GameObject> PuzzlePieces;
    public Queue<GameObject> PiecesToShoot;

    public int puzzlePieces;
    public Save save;
    public Initialize initialize;
    private LevelGenerator levelGenerator;
    private UIManager uIManager;
    private Camera mainCamera;

    void Start()
    {
        levelGenerator = GetComponent<LevelGenerator>();
        uIManager = GetComponent<UIManager>();
        Finished = false;
        mainCamera = Camera.main;
        PiecesToShoot = new Queue<GameObject>();
        initialize.finishTrigger.enabled = false;
        StartTheGame();
    }

    public void AddFillHoles(Transform lastPiece)
    {
        puzzlePieces++;
        initialize.finishTrigger.transform.position = lastPiece.position;
        if (puzzlePieces >= initialize.levels[save.level].numberOfHoles) Invoke("Finish", 1);
    }
    public void StartTheGame()
    {
        levelGenerator.Generate(initialize.levels[save.level]);
    }

    public void StartShooting()
    {
        if (PiecesToShoot.Count > 0)
        {
            var piece = PiecesToShoot.Dequeue();
            piece.transform.parent = null;
            piece.transform.DOJump(initialize.shootingPoint.transform.position,2,1, 0.8f).SetEase(Ease.InOutQuad);
            piece.GetComponent<Shooting>().allowedToShoot = true;
            piece.GetComponent<Rigidbody>().isKinematic = false;
        }
    }
    public void Finish()
    {
        Finished = true;
        mainCamera.transform.DOMove(new Vector3(0, 17, 1), 0.7f).SetEase(Ease.InOutQuad).OnComplete(()=> mainCamera.transform.DOMove(new Vector3(0, 12.5f, 1), 1.5f).SetEase(Ease.InOutQuad));        
        mainCamera.transform.DORotate(new Vector3(90, 0, 0), 1.5f).SetEase(Ease.InOutQuad);


        initialize.finishTrigger.enabled = true;
        initialize.finishTrigger.transform.DOScale(30, 4).SetDelay(1);
        uIManager.Finish();
    }

    public void NextLevel()
    {
        save.level++;
        if (save.level>=initialize.levels.Length) save.level = 0;
        SceneManager.LoadScene(0);
    }

    public void PushtoQueue(GameObject newPiece)
    {
        PiecesToShoot.Enqueue(newPiece);
        
        newPiece.transform.SetParent(initialize.queueObject.transform);
        newPiece.transform.localPosition = new Vector3(initialize.queuePos, newPiece.transform.position.y, 0);
        initialize.queueObject.transform.DOMoveX(initialize.queueObject.transform.position.x - 1.5f, 0.5f).SetEase(Ease.OutBack);
        newPiece.transform.DOScale(1, 0.5f);
        newPiece.transform.DOLocalMoveY(0, 0.8f).SetEase(Ease.OutBack);
        newPiece.transform.DORotate(Vector3.zero, 0.8f);

        var PieceShooting = newPiece.GetComponent<Shooting>();
        PieceShooting.anim.applyRootMotion = false;
        PieceShooting.StartWaitingMethod();
        newPiece.GetComponent<Rigidbody>().isKinematic = true;
        initialize.queuePos += 1.5f;
    }
}
[System.Serializable]
public class Initialize
{
    public LevelSenario[] levels;
    public Collider finishTrigger;
    public GameObject shootingPoint;
    public ParticleSystem Portal;
    public GameObject queueObject;
    [HideInInspector]
    public float queuePos=0;
}

public enum PieceType
{
    Apple,
    Apple30,
    Apple30Flip,
    Avocado,
    Avocado30,
    Avocado30Flip,
    Kiwi,
    Kiwi30,
    Kiwi30Flip,
    Lemon,
    Lemon30,
    Lemon30Flip,
    Orange,
    Orange30,
    Orange30Flip,
    Pineapple,
    Pineapple30,
    Pineapple30Flip,
    Hexa,
    Pear
}
