using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    public FinishUI finishUI;
    void Start()
    {
        
    }

    public void Finish()
    {
        finishUI.finishMessage.transform.DOScale(2, 1f).SetEase(Ease.OutBack).SetDelay(3f);
        finishUI.nextButton.transform.DOScale(1, 1f).SetEase(Ease.OutBack).SetDelay(3.5f);
    }
}
[System.Serializable]
public class FinishUI
{
    public GameObject finishMessage;
    public GameObject nextButton;
}
