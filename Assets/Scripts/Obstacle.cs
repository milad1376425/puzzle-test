using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Obstacle : MonoBehaviour
{
    public ObstacleType myType;
    private Vector3 initPos;
    
    void Start()
    {
        initPos = transform.position;
        StartCoroutine(waitingforFinish());
        if (myType == ObstacleType.Breakable)
        {
            StartCoroutine(Vanish());
        }
    }
    IEnumerator waitingforFinish()
    {
        yield return new WaitForSeconds(5);
        while(!GameManager.Finished)
        {
            yield return new WaitForSeconds(0.3f);
        }
        transform.DOScale(0, 0.5f).SetEase(Ease.InBack).SetDelay(1);
    }

    IEnumerator Vanish()
    {
        while (true)
        {
            if (Vector3.Distance(initPos,transform.position)>0.2)
            {
                yield return new WaitForSeconds(3);
                gameObject.SetActive(false);
                break;
            }
            yield return null;
        }
    }
}
public enum ObstacleType
{
    Breakable,
    Static,
    spinner
}
