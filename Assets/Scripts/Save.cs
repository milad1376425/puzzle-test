using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Save",menuName ="Save file")]
public class Save : ScriptableObject
{
    public int level;
}
