using UnityEngine;
using System.Collections.Generic;
[CreateAssetMenu(fileName = "new Level", menuName = "Level")]
public class LevelSenario : ScriptableObject
{
    [HideInInspector]
    public bool ApplyPrefab;
    public AttributeScriptableObject[] puzzlePieces = new AttributeScriptableObject[1];
    public GameObject Ground;
    public GameObject ObsAndColl;

    public int numberOfHoles;

    //[HideInInspector]
    public Senario[] Senario = new Senario[8 * 5];
    public void Clear() => Senario = new Senario[8 * 5];

    public void ChangeIcon(int id)
    {
        var fruitnum = Senario[id].fruitNumber;
        if (Senario[id].prefabSelectedNum + 1 >= puzzlePieces[fruitnum].myType.Length)
            Senario[id].prefabSelectedNum = 0;
        else
            Senario[id].prefabSelectedNum++;

        Senario[id].icon = puzzlePieces[fruitnum].myType[Senario[id].prefabSelectedNum].icon;
    }
}
[System.Serializable]
public class Senario
{
    public int fruitNumber;
    public int prefabSelectedNum;
    public bool Filled=true;
    public Texture2D icon;
}
