using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LevelGenerator : MonoBehaviour
{
    private GameManager gameManager;

    public GameObject ParentObject;
    public GameObject ParentObjectforPieces;
    public GameObject wholeLevel;
    private int id;
    private int selectedPrefab;
    private bool deactiveStatus;
    private const int rowCount = 8;
    private const int columnCount = 5;
    private Vector2 pos;
    private GameObject objectToInstantiate;
    private GameObject objectToInstantiate2;

    private void Start()
    {
        gameManager = GetComponent<GameManager>();
    }
    public void Generate(LevelSenario level)
    {
        if (level)
        {
            pos.y = 8;
            for (int i = 0; i < rowCount; i++)
            {
                pos.x = -4;
                for (int j = 0; j < columnCount; j++)
                {
                    id = level.Senario[(i * columnCount) + j].fruitNumber;
                    selectedPrefab = level.Senario[(i * columnCount) + j].prefabSelectedNum;
                    deactiveStatus = level.Senario[(i * columnCount) + j].Filled;
                    if (level.puzzlePieces[id] == null)
                    {
                        objectToInstantiate = Instantiate(level.Ground, ParentObject.transform);
                        objectToInstantiate.transform.localPosition = new Vector3(pos.x, objectToInstantiate.transform.localPosition.y, pos.y);
                        pos.x += 2;
                        continue;
                    }
                    if (id > 0 && deactiveStatus)
                    {
                        objectToInstantiate = Instantiate(level.Ground, ParentObject.transform);
                        objectToInstantiate.transform.localPosition = new Vector3(pos.x, objectToInstantiate.transform.localPosition.y, pos.y);

                        objectToInstantiate2 = Instantiate(level.puzzlePieces[id].GetPrefab(selectedPrefab), ParentObjectforPieces.transform);
                        objectToInstantiate2.GetComponent<Shooting>().enabled = false;
                        objectToInstantiate2.transform.GetChild(0).GetComponentInChildren<Collider>().isTrigger = true;
                        objectToInstantiate2.transform.GetChild(0).GetChild(0).tag = "Filled";
                        objectToInstantiate2.transform.GetChild(0).GetComponent<Animator>().enabled = false;
                        objectToInstantiate2.transform.localPosition = new Vector3(pos.x, objectToInstantiate.transform.localPosition.y + 0.2f, pos.y);
                        objectToInstantiate2.transform.GetChild(0).localPosition = -Vector3.up * 1.5f;
                    }
                    else
                    {
                        objectToInstantiate = Instantiate(level.puzzlePieces[id].GetHolePrefab(selectedPrefab), ParentObject.transform);
                        objectToInstantiate.transform.localPosition = new Vector3(pos.x, objectToInstantiate.transform.localPosition.y, pos.y);
                        gameManager.PuzzlePieces.Add(level.puzzlePieces[id].GetPrefab(selectedPrefab));
                    }
                    pos.x += 2;
                }
                pos.y -= 2;
            }
            pos.x = 0; pos.y = -8.5f;

            StartCoroutine(SpawnPieces());
            if (level.ObsAndColl)
                Instantiate(level.ObsAndColl, wholeLevel.transform);
            gameManager.Invoke("StartShooting", 3);
        }
        else Debug.Log("previous level was the final Level!");
    }

    IEnumerator SpawnPieces()
    {
        int counter = 0;

        for (int i = 0; i < 6; i++)
        {
            objectToInstantiate = Instantiate(gameManager.PuzzlePieces[counter++]);
            objectToInstantiate.transform.localScale = Vector3.zero;
            objectToInstantiate.transform.localPosition = new Vector3(10, objectToInstantiate.transform.localPosition.y, 20);

            gameManager.PushtoQueue(objectToInstantiate);
            if (counter >= gameManager.PuzzlePieces.Count)
            {
                counter = Random.Range(0, gameManager.PuzzlePieces.Count);
            }

            yield return new WaitForSeconds(0.5f);
        }
    }
}
