using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointAFollowing : MonoBehaviour
{
    public float sensivity;
    private Vector3 mousePos;

    void Update()
    {
        mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x / Screen.width * 1000, Input.mousePosition.y / Screen.height * 2000, 15));
        transform.position = mousePos;
        transform.position = new Vector3(mousePos.x, 1, mousePos.z);
    }
}
