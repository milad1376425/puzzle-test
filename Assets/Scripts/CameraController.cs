using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CameraController : MonoBehaviour
{
    public void GoToFinish(Vector3 movePos, Vector3 rotPos)
    {
        transform.DOMove(movePos, 1.5f).SetEase(Ease.InOutQuad).SetAutoKill(false);
        transform.DORotate(rotPos, 1.5f).SetEase(Ease.InOutQuad).SetAutoKill(false);
    }
}
